function include(scriptUrl) {
    document.write('<script src="' + scriptUrl + '"></script>');
}

function isIE() {
    var myNav = navigator.userAgent.toLowerCase();
    return (myNav.indexOf('msie') != -1) ? parseInt(myNav.split('msie')[1]) : false;
};

/* cookie.JS
 ========================================================*/
//include('assets/jquery.cookie.js');

/* Easing library
 ========================================================*/
//include('assets/jquery.easing.1.3.js');

/* Stick up menus
 ========================================================*/
;
$(document).ready((function ($) {
    var o = $('html');
    if (o.hasClass('desktop')) {
        //include('assets/tmstickup.js');

        $(document).ready(function () {
            $('#stuck_container').TMStickUp({})
        });
    }
})(jQuery));

// Serach form sticky fix
$(document).ready((function($) {
    var header_h = $('header').height();
    $(window).scroll(function() {
        if ($(window).scrollTop() > header_h) {
            $('.search-form.on').addClass('fixed');
        } else {
            $('.search-form.on').removeClass('fixed');
        }
    });
})(jQuery));


/* ToTop
 ========================================================*/
;
$(document).ready((function ($) {
    var o = $('html');
    if (o.hasClass('desktop')) {
        //include('assets/jquery.ui.totop.js');

        $(document).ready(function () {
            $().UItoTop({
                easingType: 'easeOutQuart',
                containerClass: 'toTop fa fa-arrow-circle-top'
            });
        });
    }
})(jQuery));

/* SMOOTH SCROLLIG
 ========================================================
;
$(document).ready((function ($) {
    var o = $('html');
    if (o.hasClass('desktop')) {
        //include('assets/jquery.mousewheel.min.js');
        //include('assets/jquery.simplr.smoothscroll.min.js');

        $(document).ready(function () {
            $.srSmoothscroll({
                step: 150,
                speed: 800
            });
        });
    }
})(jQuery));*/

/* Copyright Year
 ========================================================*/
;
$(document).ready((function ($) {
    var currentYear = (new Date).getFullYear();
    $(document).ready(function () {
        $("#copyright-year").text((new Date).getFullYear());
    });
})(jQuery));

/* Google Map
 ========================================================*/

$(document).ready((function maps($) {
	include('http://maps.google.com/maps/api/js?sensor=false');
    var o = document.getElementById("google-map");
    if (o) {
        
        //include('assets/jquery.rd-google-map.js');

        $(document).ready(function () {
            var o = $('#google-map');
            if (o.length > 0) {
                o.googleMap();
            }
        });
    }
})
(jQuery));

/* WOW
 ========================================================*/
;
$(document).ready((function ($) {
    var o = $('html');

    if ((navigator.userAgent.toLowerCase().indexOf('msie') == -1 ) || (isIE() && isIE() > 9)) {
        if (o.hasClass('desktop')) {
            //include('assets/wow.js');

            $(document).ready(function () {
                new WOW().init();
            });
        }
    }
})(jQuery));

/* Orientation tablet fix
 ========================================================*/
$(function () {
    // IPad/IPhone
    var viewportmeta = document.querySelector && document.querySelector('meta[name="viewport"]'),
        ua = navigator.userAgent,

        gestureStart = function () {
            viewportmeta.content = "width=device-width, minimum-scale=0.25, maximum-scale=1.6, initial-scale=1.0";
        },

        scaleFix = function () {
            if (viewportmeta && /iPhone|iPad/.test(ua) && !/Opera Mini/.test(ua)) {
                viewportmeta.content = "width=device-width, minimum-scale=1.0, maximum-scale=1.0";
                document.addEventListener("gesturestart", gestureStart, false);
            }
        };

    scaleFix();
    // Menu Android
    if (window.orientation != undefined) {
        var regM = /ipod|ipad|iphone/gi,
            result = ua.match(regM);
        if (!result) {
            $('.sf-menus li').each(function () {
                if ($(">ul", this)[0]) {
                    $(">a", this).toggle(
                        function () {
                            return false;
                        },
                        function () {
                            window.location.href = $(this).attr("href");
                        }
                    );
                }
            })
        }
    }
});
var ua = navigator.userAgent.toLocaleLowerCase(),
    regV = /ipod|ipad|iphone/gi,
    result = ua.match(regV),
    userScale = "";
if (!result) {
    userScale = ",user-scalable=0"
}
document.write('<meta name="viewport" content="width=device-width,initial-scale=1.0' + userScale + '">');
/* FancyBox
 ========================================================*/
;
$(document).ready((function($) {
    var o = $('.thumb');
    if (o.length > 0) {
        //include('assets/jquery.fancybox.js');
        //include('assets/jquery.fancybox1-media.js');
        //include('assets/jquery.fancybox-buttons.js');
        $(document).ready(function() {
            o.fancybox();
        });
    }
})(jQuery));

 

