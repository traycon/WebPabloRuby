class ArticulosController < ApplicationController
  before_action :require_login
  before_action :set_articulo, only: [:show, :edit, :update, :destroy]

def require_login
     current_user
    unless @current_user!=nil
      redirect_to "http://www.ventanastalleresmazo.com/login"
    end
  end
  # GET /articulos
  # GET /articulos.json
  def index
    current_user
    @articulos = Articulo.all
  end

  # GET /articulos/1
  # GET /articulos/1.json
  def show
    current_user
  end

  # GET /articulos/new
  def new
    current_user
    @articulo = Articulo.new
  end

  # GET /articulos/1/edit
  def edit
    current_user
  end

  # POST /articulos
  # POST /articulos.json
  def create
    current_user
    foto=params[:articulo][:foto]
    nombreFoto=Time.now.to_i.to_s+foto.original_filename
    File.open(Rails.root.join('public', 'uploads', nombreFoto), 'wb') do |file|
    file.write(foto.read)
  end
    @articulo = Articulo.new(articulo_params)
    @articulo.foto=nombreFoto
    
    respond_to do |format|
      if @articulo.save
        format.html { redirect_to @articulo, notice: 'Articulo was successfully created.' }
        format.json { render :show, status: :created, location: @articulo }
      else
        format.html { render :new }
        format.json { render json: @articulo.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /articulos/1
  # PATCH/PUT /articulos/1.json
  def update
    current_user
    foto=params[:articulo][:foto]
    nombreFoto=nil
    if foto!=nil
    nombreFoto=Time.now.to_i.to_s+foto.original_filename
    File.open(Rails.root.join('public', 'uploads', nombreFoto), 'wb') do |file|
    file.write(foto.read)
    end
    
    ruta_al_archivo = Rails.root.join('public', 'uploads', @articulo.foto);
   if File.exist?(ruta_al_archivo)
      File.delete(ruta_al_archivo);
   end
  
    end
    
    respond_to do |format|
      
      if @articulo.update(articulo_params)
        if nombreFoto!=nil
          @articulo.foto=nombreFoto
          @articulo.save
        end
        format.html { redirect_to @articulo, notice: 'Articulo was successfully updated.' }
        format.json { render :show, status: :ok, location: @articulo }
      else
        format.html { render :edit }
        format.json { render json: @articulo.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /articulos/1
  # DELETE /articulos/1.json
  def destroy
    current_user
    ruta_al_archivo = Rails.root.join('public', 'uploads', @articulo.foto);
   if File.exist?(ruta_al_archivo)
      File.delete(ruta_al_archivo);
   end
    @articulo.destroy
    respond_to do |format|
      format.html { redirect_to "http://www.ventanastalleresmazo.com/articulos", notice: 'Articulo was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_articulo
      @articulo = Articulo.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def articulo_params
      params.require(:articulo).permit(:foto, :tituloEs, :tituloEn, :tituloFr, :tituloDe, :subtituloEs, :subtituloEn, :subtituloFr, :subtituloDe, :descripcionEs, :descripcionEn, :descripcionFr, :descripcionDe)
    end
end
