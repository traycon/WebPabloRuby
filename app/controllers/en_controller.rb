class EnController < ApplicationController
  def home
    current_user
    @traducciones=Diccionario.all
  end
  def nuestraEmpresa
    current_user
        @traducciones=Diccionario.all
  end
  def nuestrosProductos
    current_user
        @traducciones=Diccionario.all
  end
  def garantia
    current_user
        @traducciones=Diccionario.all
  end
  def materialesCalidad
    current_user
        @traducciones=Diccionario.all
  end
  def productos
    current_user
        @traducciones=Diccionario.all
  end
  def ventanas
    current_user
        @traducciones=Diccionario.all
  end
  def puertas
    current_user
        @traducciones=Diccionario.all
  end
  def cerramientos
    current_user
        @traducciones=Diccionario.all
  end
  def cristalInteligente
    current_user
        @traducciones=Diccionario.all
  end
  def trabajos
    current_user
        @traducciones=Diccionario.all
    @trabajos = Trabajo.all.sort_by{|e| e[:nombreTrabajo]}
  end
  def blog
    current_user
    @articulosBlog=Articulo.all.reverse
  end
  def articuloBlog
    current_user
    @articulo=Articulo.find(params[:id])
  end
  def coloresFoliados
    current_user
        @traducciones=Diccionario.all
  end
end
