class EsController < ApplicationController
  def home
    current_user
  end
  def nuestraEmpresa
    current_user
  end
  def nuestrosProductos
    current_user
  end
  def garantia
    current_user
  end
  def materialesCalidad
    current_user
  end
  def productos
    current_user
  end
  def ventanas
    current_user
  end
  def puertas
    current_user
  end
  def cerramientos
    current_user
  end
  def cristalInteligente
    current_user
  end
  def trabajos
    current_user
    @trabajos = Trabajo.all.sort_by{|e| e[:nombreTrabajo]}
  end
  def blog
    current_user
    @articulosBlog=Articulo.all.reverse
  end
  def articuloBlog
    current_user
    @articulo=Articulo.find(params[:id])
  end
  def coloresFoliados
    current_user
  end
  def avisoLegal
    current_user
  end
  def login
    current_user
    @error=""
    @user= Usuario.new
  end
  def entrar
    user = Usuario.find_by(username: params[:usuario][:username])
    if user == nil
      @error="Información incorrecta"
      @user=Usuario.new
      render 'login'
    else
    if user.password.eql? params[:usuario][:password]
      log_in user
      current_user
      if @current_user.tipo=="Administrador"
      redirect_to usuarios_path
      end
      if @current_user.tipo=="Traductor"
        redirect_to diccionarios_path
      end
      if @current_user.tipo=="Blogger"
        redirect_to articulos_path
      end
    else
      @error="Información incorrecta"
      @user=Usuario.new
      render 'login'
    end
  end
  end
  def salir
    log_out
    redirect_to es_inicio_path
  end
end
