class TrabajosController < ApplicationController
  before_action :require_login
  before_action :set_trabajo, only: [:show, :edit, :update, :destroy]
  
def require_login
     current_user
    unless @current_user!=nil
      redirect_to "http://www.ventanastalleresmazo.com/login"
    end
  end
  # GET /trabajos
  # GET /trabajos.json
  def index
    current_user
    @trabajos = Trabajo.all.sort_by{|e| e[:nombreTrabajo]}
  end

  # GET /trabajos/1
  # GET /trabajos/1.json
  def show
    current_user
  end

  # GET /trabajos/new
  def new
    current_user
    @trabajo = Trabajo.new
  end

  # GET /trabajos/1/edit
  def edit
    current_user
  end

  # POST /trabajos
  # POST /trabajos.json
  def create
    
current_user
    foto=params[:file]
    nombreFoto=Time.now.to_i.to_s+foto.original_filename
    File.open(Rails.root.join('public', 'uploads', nombreFoto), 'wb') do |file|
    file.write(foto.read)
  end
  @trabajo = Trabajo.new
  @trabajo.foto=nombreFoto
  @trabajo.nombreTrabajo=params[:trabajo][:nombreTrabajo]
    respond_to do |format|
      if @trabajo.save
        format.html { redirect_to @trabajo, notice: 'Trabajo was successfully created.' }
        format.json { render :show, status: :created, location: @trabajo }
      else
        format.html { render :new }
        format.json { render json: @trabajo.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /trabajos/1
  # PATCH/PUT /trabajos/1.json
  def update
    current_user
    respond_to do |format|
      if @trabajo.update(trabajo_params)
        format.html { redirect_to @trabajo, notice: 'Trabajo was successfully updated.' }
        format.json { render :show, status: :ok, location: @trabajo }
      else
        format.html { render :edit }
        format.json { render json: @trabajo.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /trabajos/1
  # DELETE /trabajos/1.json
  def destroy
    current_user
        ruta_al_archivo = Rails.root.join('public', 'uploads', @trabajo.foto);
   if File.exist?(ruta_al_archivo)
      File.delete(ruta_al_archivo);
   end
    @trabajo.destroy
    respond_to do |format|
      format.html { redirect_to "http://www.ventanastalleresmazo.com/trabajos", notice: 'Trabajo was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_trabajo
      @trabajo = Trabajo.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def trabajo_params
      params.require(:trabajo).permit(:foto)
    end
end
