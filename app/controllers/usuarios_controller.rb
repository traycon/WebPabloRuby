class UsuariosController < ApplicationController
  before_action :require_login
  before_action :set_usuario, only: [:show, :edit, :update, :destroy]

def require_login
     current_user
    unless @current_user!=nil
      redirect_to "http://www.ventanastalleresmazo.com/login"
    end
  end
  # GET /usuarios
  # GET /usuarios.json
  def index
    current_user
    @usuarios = Usuario.all
  end

  # GET /usuarios/1
  # GET /usuarios/1.json
  def show
    current_user
  end

  # GET /usuarios/new
  def new
    current_user
    @usuario = Usuario.new
  end

  # GET /usuarios/1/edit
  def edit
    current_user
  end

  # POST /usuarios
  # POST /usuarios.json
  def create
    current_user
    @usuario = Usuario.new(usuario_params)

    respond_to do |format|
      if @usuario.save
        format.html { redirect_to @usuario, notice: 'Usuario was successfully created.' }
        format.json { render :show, status: :created, location: @usuario }
      else
        format.html { render :new }
        format.json { render json: @usuario.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /usuarios/1
  # PATCH/PUT /usuarios/1.json
  def update
    current_user
    respond_to do |format|
      if @usuario.update(usuario_params)
        format.html { redirect_to @usuario, notice: 'Usuario was successfully updated.' }
        format.json { render :show, status: :ok, location: @usuario }
      else
        format.html { render :edit }
        format.json { render json: @usuario.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /usuarios/1
  # DELETE /usuarios/1.json
  def destroy
    current_user
    @usuario.destroy
    respond_to do |format|
      format.html { redirect_to "http://www.ventanastalleresmazo.com/usuarios", notice: 'Usuario was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_usuario
      @usuario = Usuario.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def usuario_params
      params.require(:usuario).permit(:username, :password, :tipo)
    end
end
