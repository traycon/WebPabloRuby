module ApplicationHelper
  def activo(inicio, acerca, proveedores, productos, trabajos, blog)
  content_for :inicio, inicio.to_s
  content_for :acerca, acerca.to_s
  content_for :proveedores, proveedores.to_s
  content_for :productos, productos.to_s
  content_for :trabajos, trabajos.to_s
  content_for :blog, blog.to_s
end
def idioma(idioma)
  content_for :idioma, idioma.to_s
end

def user(user)
  content_for :user, user
  if user!=nil
  content_for :username, user["username"]
  content_for :tipo, user["tipo"]
  end
end
end
