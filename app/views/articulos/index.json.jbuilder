json.array!(@articulos) do |articulo|
  json.extract! articulo, :id, :foto, :tituloEs, :tituloEn, :tituloFr, :tituloDe, :subtituloEs, :subtituloEn, :subtituloFr, :subtituloDe, :descripcionEs, :descripcionEn, :descripcionFr, :descripcionDe
  json.url articulo_url(articulo, format: :json)
end
