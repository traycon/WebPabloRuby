json.array!(@diccionarios) do |diccionario|
  json.extract! diccionario, :id, :espanol, :ingles, :frances, :almenan
  json.url diccionario_url(diccionario, format: :json)
end
