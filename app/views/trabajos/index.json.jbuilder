json.array!(@trabajos) do |trabajo|
  json.extract! trabajo, :id, :foto
  json.url trabajo_url(trabajo, format: :json)
end
