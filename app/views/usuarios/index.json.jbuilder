json.array!(@usuarios) do |usuario|
  json.extract! usuario, :id, :username, :password, :tipo
  json.url usuario_url(usuario, format: :json)
end
