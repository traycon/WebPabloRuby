# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path
# Rails.application.config.assets.paths << Emoji.images_path

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
# Rails.application.config.assets.precompile += %w( search.js )
Rails.application.config.assets.precompile += %w( 0-jquery.js )
Rails.application.config.assets.precompile += %w( 1-jquery-migrate-1.2.1.js )
Rails.application.config.assets.precompile += %w( 2-script.js )
Rails.application.config.assets.precompile += %w( 3-jquery.cookie.js )
Rails.application.config.assets.precompile += %w( 4-jquery.easing.1.3.js )
Rails.application.config.assets.precompile += %w( 5-superfish.js)
Rails.application.config.assets.precompile += %w( 6-jquery.rd-navbar.js )
Rails.application.config.assets.precompile += %w( 7-device.min.js )
Rails.application.config.assets.precompile += %w( 8-tmstickup.js )
Rails.application.config.assets.precompile += %w( 9-jquery.ui.totop.js )
Rails.application.config.assets.precompile += %w( a1-jquery.mousewheel.min.js )
Rails.application.config.assets.precompile += %w( a2-jquery.simplr.smoothscroll.min.js )
Rails.application.config.assets.precompile += %w( a3-jquery.rd-google-map.js )
Rails.application.config.assets.precompile += %w( a4-wow.js )
Rails.application.config.assets.precompile += %w( a7-jquery.mobile.customized.min.js )
Rails.application.config.assets.precompile += %w( a9-jquery.fancybox.js )
Rails.application.config.assets.precompile += %w( b1-jquery.fancybox-media.js )
Rails.application.config.assets.precompile += %w( b2-jquery.fancybox-buttons.js )
Rails.application.config.assets.precompile += %w( dropzone.js )
Rails.application.config.assets.precompile += %w( jquery.tooltipster.min.js )