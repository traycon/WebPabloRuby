Rails.application.routes.draw do
  resources :trabajos
  resources :usuarios
  resources :diccionarios
  resources :articulos
  
   #RUTAS ESPAÑOL
  root :to => redirect('/es/inicio')
  get 'es/inicio' => 'es#home'
  get 'es/colores-y-foliados'=> 'es#coloresFoliados'
  get 'es/nuestraEmpresa'=> 'es#nuestraEmpresa'
  get 'es/nuestrosProductos'=> 'es#nuestrosProductos'
  get 'es/garantia'=> 'es#garantia'
  get 'es/materialesCalidad'=> 'es#materialesCalidad'
  get 'es/productos'=> 'es#productos'
  get 'es/ventanas'=> 'es#ventanas'
  get 'es/puertas'=> 'es#puertas'
  get 'es/cerramientos'=> 'es#cerramientos'
  get 'es/cristalInteligente'=> 'es#cristalInteligente'
  get 'es/trabajos'=> 'es#trabajos'
  get 'es/blog'=> 'es#blog'
  get 'es/articuloBlog/:id'=> 'es#articuloBlog', as: :articuloBlogEs
  get 'es/avisoLegal' => 'es#avisoLegal'
  #RUTAS INGLES
    get 'en/home' => 'en#home'
  get 'en/colors-and-foliated'=> 'en#coloresFoliados'
  get 'en/ourCompany'=> 'en#nuestraEmpresa'
  get 'en/ourProducts'=> 'en#nuestrosProductos'
  get 'en/warranty'=> 'en#garantia'
  get 'en/qualityMaterials'=> 'en#materialesCalidad'
  get 'en/products'=> 'en#productos'
  get 'en/windows'=> 'en#ventanas'
  get 'en/doors'=> 'en#puertas'
  get 'en/enclosures'=> 'en#cerramientos'
  get 'en/intelligentGlass'=> 'en#cristalInteligente'
  get 'en/works'=> 'en#trabajos'
  get 'en/blog'=> 'en#blog'
  get 'en/articleBlog/:id'=> 'en#articuloBlog', as: :articuloBlogEn
   #RUTAS FRANCES
    get 'fr/home' => 'fr#home'
  get 'fr/couleurs-et-feuillete'=> 'fr#coloresFoliados'
  get 'fr/notreSociete'=> 'fr#nuestraEmpresa'
  get 'fr/nosProduits'=> 'fr#nuestrosProductos'
  get 'fr/garantie'=> 'fr#garantia'
  get 'fr/materiauxQualite'=> 'fr#materialesCalidad'
  get 'fr/produits'=> 'fr#productos'
  get 'fr/fenetres'=> 'fr#ventanas'
  get 'fr/portes'=> 'fr#puertas'
  get 'fr/boitiers'=> 'fr#cerramientos'
  get 'fr/verreIntelligent'=> 'fr#cristalInteligente'
  get 'fr/travaux'=> 'fr#trabajos'
  get 'fr/blog'=> 'fr#blog'
  get 'fr/articleBlog/:id'=> 'fr#articuloBlog', as: :articuloBlogFr
   #RUTAS ALEMAN
    get 'de/home' => 'de#home'
  get 'de/farben-und-foliated'=> 'de#coloresFoliados'
  get 'de/unserUnternehmen'=> 'de#nuestraEmpresa'
  get 'de/unsereProdukte'=> 'de#nuestrosProductos'
  get 'de/garantie'=> 'de#garantia'
  get 'de/werkstoffeQualitat'=> 'de#materialesCalidad'
  get 'de/produkte'=> 'de#productos'
  get 'de/fenster'=> 'de#ventanas'
  get 'de/turen'=> 'de#puertas'
  get 'de/gehause'=> 'de#cerramientos'
  get 'de/intelligentGlas'=> 'de#cristalInteligente'
  get 'de/arbeit'=> 'de#trabajos'
  get 'de/blog'=> 'de#blog'
  get 'de/artikelBlog/:id'=> 'de#articuloBlog', as: :articuloBlogDe
  
  get 'delDiccionario/:id'=> 'diccionarios#destroy', as: :delDicionario
  get 'delUsuario/:id'=> 'usuarios#destroy', as: :delUsuario
  get 'delArticulo/:id'=> 'articulos#destroy', as: :delArticulo
  get 'delTrabajo/:id'=> 'trabajos#destroy', as: :delTrabajo
  #LOGIN
  get 'login'=>'es#login'
  post 'entrar'=>'es#entrar'
  get 'salir'=>'es#salir'
  
  get '*path' => redirect('/es/inicio')
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
