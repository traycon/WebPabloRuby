class CreateArticulos < ActiveRecord::Migration
  def change
    create_table :articulos do |t|
      t.string :foto
      t.string :tituloEs
      t.string :tituloEn
      t.string :tituloFr
      t.string :tituloDe
      t.string :subtituloEs
      t.string :subtituloEn
      t.string :subtituloFr
      t.string :subtituloDe
      t.string :descripcionEs
      t.string :descripcionEn
      t.string :descripcionFr
      t.string :descripcionDe

      t.timestamps null: false
    end
  end
end
