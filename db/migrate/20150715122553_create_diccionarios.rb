class CreateDiccionarios < ActiveRecord::Migration
  def change
    create_table :diccionarios do |t|
      t.string :espanol
      t.string :ingles
      t.string :frances
      t.string :almenan

      t.timestamps null: false
    end
  end
end
