class CreateTrabajos < ActiveRecord::Migration
  def change
    create_table :trabajos do |t|
      t.string :foto

      t.timestamps null: false
    end
  end
end
