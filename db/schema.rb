# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150726233748) do

  create_table "articulos", force: :cascade do |t|
    t.string   "foto"
    t.string   "tituloEs"
    t.string   "tituloEn"
    t.string   "tituloFr"
    t.string   "tituloDe"
    t.string   "subtituloEs"
    t.string   "subtituloEn"
    t.string   "subtituloFr"
    t.string   "subtituloDe"
    t.string   "descripcionEs"
    t.string   "descripcionEn"
    t.string   "descripcionFr"
    t.string   "descripcionDe"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "diccionarios", force: :cascade do |t|
    t.string   "espanol"
    t.string   "ingles"
    t.string   "frances"
    t.string   "almenan"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "trabajos", force: :cascade do |t|
    t.string   "foto"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.string   "nombreTrabajo"
  end

  create_table "usuarios", force: :cascade do |t|
    t.string   "username"
    t.string   "password"
    t.string   "tipo"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
