require 'test_helper'

class ArticulosControllerTest < ActionController::TestCase
  setup do
    @articulo = articulos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:articulos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create articulo" do
    assert_difference('Articulo.count') do
      post :create, articulo: { descripcionDe: @articulo.descripcionDe, descripcionEn: @articulo.descripcionEn, descripcionEs: @articulo.descripcionEs, descripcionFr: @articulo.descripcionFr, foto: @articulo.foto, subtituloDe: @articulo.subtituloDe, subtituloEn: @articulo.subtituloEn, subtituloEs: @articulo.subtituloEs, subtituloFr: @articulo.subtituloFr, tituloDe: @articulo.tituloDe, tituloEn: @articulo.tituloEn, tituloEs: @articulo.tituloEs, tituloFr: @articulo.tituloFr }
    end

    assert_redirected_to articulo_path(assigns(:articulo))
  end

  test "should show articulo" do
    get :show, id: @articulo
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @articulo
    assert_response :success
  end

  test "should update articulo" do
    patch :update, id: @articulo, articulo: { descripcionDe: @articulo.descripcionDe, descripcionEn: @articulo.descripcionEn, descripcionEs: @articulo.descripcionEs, descripcionFr: @articulo.descripcionFr, foto: @articulo.foto, subtituloDe: @articulo.subtituloDe, subtituloEn: @articulo.subtituloEn, subtituloEs: @articulo.subtituloEs, subtituloFr: @articulo.subtituloFr, tituloDe: @articulo.tituloDe, tituloEn: @articulo.tituloEn, tituloEs: @articulo.tituloEs, tituloFr: @articulo.tituloFr }
    assert_redirected_to articulo_path(assigns(:articulo))
  end

  test "should destroy articulo" do
    assert_difference('Articulo.count', -1) do
      delete :destroy, id: @articulo
    end

    assert_redirected_to articulos_path
  end
end
